package com.midterm;

public class Monster {
    int hp;

    public Monster() {
        hp = (int) (Math.random() * 11)+1;
    }

    public void stat() {
        System.out.println("-------------");
        System.out.println("Name : Monster");
        System.out.println("HP : "+hp);
        
    }

    public int getHP() {
        return hp;
    }
    
    

}
