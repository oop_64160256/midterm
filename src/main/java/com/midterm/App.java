package com.midterm;

import java.util.Scanner;

public class App {
    static Scanner sc = new Scanner(System.in);
    static Worker worker = new Worker("John", 5);
    static Monster monster = new Monster();

    public static String input() {
        return sc.next();
    }

    public static void process(String command) {
        switch (command) {
            case "att":
                worker.attack(worker.getLevel());
                break;
            case "heal":
                worker.heal();
                break;
            case "q":
                System.exit(0);
                ;
                break;
            default:
                break;
        }
    }

    public static void main(String[] args) {

        while (true) {
            System.out.println();
            worker.stat();
            String command = input();
            process(command);
            if (worker.getStamina() == 0) {
                System.out.println("Game Over");
                System.exit(0);
                ;
            }
        }

    }
}
