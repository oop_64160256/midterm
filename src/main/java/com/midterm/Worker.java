package com.midterm;

public class Worker {
    private String name;
    private int stamina;
    private int level;
    private Monster monster = new Monster();
    private int full_stamina;
    int atkCount = 0;
    int mons_hp = monster.getHP();

    public Worker(String name, int stamina) {
        this.stamina = stamina;
        full_stamina = stamina;
        this.name = name;
        this.level = 1;
    }

    public void attack(int level) {
        mons_hp -= level;
        stamina -= 1;
        atkCount += 1;
        if (atkCount > 3) {
            this.level += 1;
            atkCount = 0;
        }
    }

    public void stat() {
        System.out.println("-------------");
        System.out.println("Name : " + name);
        System.out.println("Stamina : " + stamina);
        System.out.println("Level : " + level);
        System.out.println("-------------");
        System.out.println("Name : monster");
        System.out.println("HP : "+mons_hp);
    }

    public int getLevel() {
        return level;
    }

    public void heal() {
        this.stamina = full_stamina;
    }

    public int getStamina() {
        return stamina;
    }

    public int getHP() {
        return mons_hp;
    }
}
